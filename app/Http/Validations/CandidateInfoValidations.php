<?php
namespace app\Http\Validations;

use Validator;

class CandidateInfoValidations
{
    /**
     * Master Circle Area Validation
     */

	public static function validate ($request, $id = 0)
	{
		$validator = Validator::make($request->all(), [
		    'name'         => 'required',
		    'email'        => 'required',
		    'gender'       => 'required',
		    'skils'        => 'required',
	   
		]);

	if ($validator->fails()) {
	   return ([
	       'success' => false,
	       'errors' => $validator->errors()
	   ]);
	 }

	 return ['success'=> 'true'];

	}     
}