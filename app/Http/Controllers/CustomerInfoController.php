<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\Skils;
use App\CustomerInfo;
use Illuminate\Http\Request;
use App\Helpers\GlobalFileUploadFunctoin;
use DB;

class CustomerInfoController extends Controller
{
     public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * get all candidate Info
     */
    public function index()
    {
    	$pageTitle = "Candidate informations list";
        $query =  $query = CustomerInfo::with('customerSkils')
                                        ->where('deleted_at',null);
        $list = $query->get();

        return view('candidate.list', [
            'data' => $list,
            'message' => 'Candidate informations list',
            'pageTitle' => $pageTitle
        ]);
    }


     public function deleteList()
    {
        $pageTitle = "Candidate informations deleted list";
        $query =  $query = CustomerInfo::onlyTrashed();
        $list = $query->get();
        return view('candidate.list', [
            'data' => $list,
            'message' => 'Data Not found',
            'pageTitle' => $pageTitle
        ]);
    }

    

    /**
     * Create candidate Info
     */
    public function createCandidate()
    {
        $pageTitle = "Create a candidate";

        return view('candidate.create', [
            'pageTitle' => $pageTitle
        ]);
    }
    /**
     * Customer Info store
     */
    public function store(Request $request)
    {
    	$pageTitle ="Store Data";


       /* $validateResult = $request->validate([
            'name'      => 'required',
            'email'     => 'required',
            'gender'    => 'required',
            'cost'      => 'required'
        ]);

            dd($validateResult, 'header(string)');

        if ($validateResult->fails()) {
           return redirect()->back()
            ->with('message', 'Project updated successfully');
        }*/

           
       
        $file_path 		= 'task-customer-info';
        $image 			=  $request->file('image');		
		$skils      	=  $request->skils;

        DB::beginTransaction();
        try {
			$customerInfo                = new CustomerInfo();
			$customerInfo->name       	=  $request->name;
			$customerInfo->email       	=  $request->email;
			$customerInfo->gender      	=  $request->gender;
        dd($customerInfo);

		 	if($image !=null && $image !=""){
                $image_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path,'image');
            }

			$customerInfo->image       	=  $image_name ? $image_name : null;

			if($customerInfo->save()){
				$customer_info_id = $customerInfo->id; 
				foreach ($skils as $key => $skil) {
					$skils                     = new Skils();
					$skils->customer_info_id   =  $customer_info_id;
					$skils->skils_title        =  $skil;
                    $skils->save();
				}

				GlobalFileUploadFunctoin::file_upload($request, $file_path, 'image', $image_name);
			}


			DB::commit();

        } catch (\Exception $ex) {
            DB::rollback();
            //Session::flash('danger', $e->getMessage());
        }

        return view('candidate.index', [
            'data' => $customerInfo,
            'message' => 'Data save successfully',
            'pageTitle' => $pageTitle
        ]);
       
    }

       /**
     * Create candidate Info
     */
    public function editCandidate($id)
    {
        $pageTitle = "Edit a candidate";
        $customerInfo   = CustomerInfo::find($id);

        //dd($customerInfo);

        return view('candidate.edit', [
            'pageTitle' => $pageTitle,
            'data' => $customerInfo,
        ]);
    }

    public function update(Request $request, $id)
    {
       

        $file_path 		= 'candidate-info';
        $image 			=  $request->file('image');

        $customerInfo 	= CustomerInfo::find($id);
        $old_file 		= $customerInfo->image;

        if (!$customerInfo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        DB::beginTransaction();

        try {
			$customerInfo->name       	=  $request->name??$customerInfo->name;
			$customerInfo->email       	=  $request->email??$customerInfo->email;
			$customerInfo->gender      	=  $request->gender??$customerInfo->gender;

		 	if($image !=null && $image !=""){
                $image_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request,$file_path,'image');
				$customerInfo->image    =  $image_name;
            }

			if($customerInfo->save()){
				 GlobalFileUploadFunctoin::file_upload($request, $file_path, 'image', $image_name, $old_file );

				$oldSkilsDelete= Skils::where('customer_info_id',$id)->delete();

			 	
			 	if($oldSkilsDelete){
					foreach ($skils as $key => $skil) {

						$skils                		= new Skils();
						$skils->customer_info_id       	=  $id;
						$skils->skils_title       		=  $skil;
					}			 		
			 	}
			}

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            Session::flash('danger', $e->getMessage());
        }
        return redirect()->back();
        
    }

    /**
     * Customer Info moveToTrush
     */
    public function moveToTrush($id)
    {      

    	$customerInfo = CustomerInfo::find($id);        
        $customerInfo->delete();

        return redirect()->back();      
    }
    
 	
    public function restoreData($id)
    {
    	$pageTitle ="";        
        $customerInfo = CustomerInfo::withTrashed()
                         ->where('id', $id)
                         ->restore();
        
        return redirect()->route('infoList');
        
    }


      /**
     * Customer Info permanentlyDelete
     */
    public function permanentlyDelete($id)
    {
        $customerInfo = CustomerInfo::withTrashed()
                         ->where('id', $id)
                         ->forceDelete();
        
        //$customerInfo->forceDelete();
        return redirect()->back();
    }
}
