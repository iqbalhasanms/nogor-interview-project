<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerInfo extends Model
{

    protected $table="customer_info";

    use SoftDeletes;

    

    protected $fillable =[ 
		'name',	
		'email',	
		'image',	
		'gender',	
		'skils'
    ];


    public function customerSkils(){
    	return $this->hasMany(Skils::class);
    }
}


