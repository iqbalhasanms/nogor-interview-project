<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skils extends Model
{
     protected $table="table_skils";

    protected $fillable =[ 
		'candidate_info_id',	
		'skils_title'
    ];
}

