<?php
namespace App\Helpers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator;


class GlobalFileUploadFunctoin{


  /*Check Dir Permission*/
    public static function is_dir_set_permission($directory){
        if(is_dir($directory)) {
            GlobalFileUploadFunctoin::check_permission($directory);
            return true;
        }
        else
        {
            GlobalFileUploadFunctoin::make_directory($directory);
            return true;
        }

    }

   /*Make Diretory*/
    protected static function make_directory($directory){
        File::makeDirectory($directory, 0755, true, true);
        return true;
    }

  /*Check Permission*/
    protected static function check_permission($directory){
        if(is_writable($directory))
        {
            return true;
        }
        else
        {
            GlobalFileUploadFunctoin::set_permission($directory);
            return true;
        }
    }
 /*Set Permission*/
    protected static function set_permission($directory){
        if(!is_dir($directory)){
            File::makeDirectory($directory, 0777, true, true);
            return true;
        }
        return false;
    }



/*+++++File validation+++++++*/
    public static function file_validation_and_return_file_name($request,$file_path,$uploads_name){

        $file = $request->file($uploads_name);
        $rules = array( $uploads_name => 'required|mimes:png,gif,jpeg,svg,tiff,pdf,doc,docx,tex,txt,rtf');
        $validator = Validator::make(array($uploads_name => $file), $rules);
        if ($validator->passes()) {
            $file_name =time().'.' . $file->getClientOriginalExtension();
        }
        if ($validator->fails()) {
            return ([
                'success' => false,
                'errors' => $validator->errors()
            ]);
        }
        return $file_name;



    }


    /*+++++Upload File++++++++++*/
    public static function file_upload($request, $file_path, $uploads_name, $file_name, $old_file=null){
        $file = $request->file($uploads_name);
        $fileDestinationPath = storage_path('uploads/'.$file_path.'/original/');

         if ($file !=null) {
                if($file != "") {
                      if(file_exists($fileDestinationPath.'/'.$old_file) && $old_file != null ){
                            unlink($fileDestinationPath.'/'.$old_file);
                        }
                    GlobalFileUploadFunctoin::is_dir_set_permission($fileDestinationPath);
                    $file->move($fileDestinationPath,$file_name);
                }
            }
    }





}
