@extends('layouts.master')

@section('content')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{!! $pageTitle !!}</div>

                <div class="card-body">
                    
                    @if(isset($data) && count($data) > 0)
                        <table class="table table-hover">
                            <thead>
                              <tr>
                                <th>Name</th>
                                <th>Photo</th>
                                <th>Email</th>
                                <th>Gender</th>
                                <th>Skils</th>
                                <th>Action</th>
                              </tr>
                            </thead>
                            <tbody>                     
                                @foreach($data as $key => $value)
                                <tr>
                                    <td>{!! $value->name !!}</td>                               
                                    <td> <img src="{{ storage_path().'/uploads/candidate-info/original/'.$value->image }}" width=""></td>                               
                                    <td>{!! $value->email !!}</td>                              
                                    <td>{!! $value->gender !!}</td>                             
                                    <td>
                                        <ul class="skils">                                      
                                            @foreach($value->candidateSkils as $k => $val)
                                                <li class="item">{!! $val->skils_title !!}</li>
                                            @endforeach                 
                                        </ul>                                   
                                    </td>

                                    <td> 
                                        @if($value->deleted_at == null)
                                            <a href="{{ route('editCandidate',$value->id ) }}">Edit</a> 
                                            <a href="{{ route('infoMoveToTrush',$value->id ) }}">Delete</a>
                                        @else
                                            <a href="{{ route('infoRestoreData',$value->id ) }}">  Restore</a>
                                            <a href="{{ route('infoPermanentlyDelete',$value->id ) }}"> Parmenently Delete</a>
                                            
                                        @endif
                                    </td>                             
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                     <h2 style="text-align: center; color: #f20000;">Data Not found !!</h2>   
                    @endif
                </div>
            </div>
        </div>
   
@endsection
