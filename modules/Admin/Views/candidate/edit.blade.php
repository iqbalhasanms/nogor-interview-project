@extends('layouts.master')

@section('content')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{!! $pageTitle !!}</div>
                <div class="card-body">
                    @if(isset($data))                    
                    <form method="POST" action="{{ route('infoUpdate',$data->id) }}" enctype="multipart/form-data">    
                       @csrf                       

                        <div class="form-group">
                          <label for="name">Name:</label>
                          <input type="text" class="form-control" id="name" value="{{$data->name}}" name="name">
                        </div>

                        <div class="form-group">
                          <label for="email">Email:</label>
                          <input type="email" class="form-control" id="email" value="{{$data->email}}" name="email">
                        </div>

                        <div class="form-group">
                            <input type="file" class="form-control" name="image" />
                        </div>  
                        <div class="form-group">
                            <label for="gender">Gender:</label>
                            <div  id="gender">
                                <input type="radio" @if($data->gender =='male') checked @endif  name="gender" value="male"> Male
                                <input type="radio" @if($data->gender =='female') checked @endif name="gender" value="female"> Female                   
                            </div>                
                        </div>


                        <div class="form-group">
                            <label for="gender">Skils:</label>
                            <div  id="gender">
                                <?php $skils =['laravel','codeigniter','ajax','vuejs','mysql','api'];
                                    $candidateSkils = $data->candidateSkils;
                                    $previousSkils = [];
                                    foreach ($skils as $key => $skil) {
                                        $previousSkils[$key] = $candidateSkils[$key]->skils_title??null;
                                    ?>
                                    <input type="checkbox"  name="skils[]" value="<?php $skil; ?>" @if($previousSkils[$key]== $skil) checked @endif>
                                   {!! ucfirst($skil) !!}
                                <?php } ?>                
                            </div>                
                        </div>                       
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                    @endif
                </div>
            </div>
        </div>
   
@endsection

