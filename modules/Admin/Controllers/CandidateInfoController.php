<?php
namespace Modules\Admin\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Modules\Admin\Models\CandidateInfo;
use Modules\Admin\Models\Skils;
use Illuminate\Http\Request;
use App\Helpers\GlobalFileUploadFunctoin;
use App\Http\Validations\CandidateInfoValidations;
use DB;



class CandidateInfoController extends Controller
{
     public function __construct()
    {
       $this->middleware('auth');
    }

    /**
     * get all candidate Info
     */
    public function index()
    {
    	$pageTitle = "Candidate informations list";
        $query =  CandidateInfo::with('candidateSkils')
                                        ->where('deleted_at',null);
        $list = $query->get();

        return view('Admin::candidate.list', [
            'data' => $list,
            'message' => 'Candidate informations list',
            'pageTitle' => $pageTitle
        ]);
    }


     public function deleteList()
    {
        $pageTitle = "Candidate informations deleted list";
        $query =  $query = CandidateInfo::onlyTrashed();
        $list = $query->get();
        return view('Admin::candidate.list', [
            'data' => $list,
            'message' => 'Data Not found',
            'pageTitle' => $pageTitle
        ]);
    }

    

    /**
     * Create candidate Info
     */
    public function createCandidate()
    {
        $pageTitle = "Create a candidate";

        return view('Admin::candidate.create', [
            'pageTitle' => $pageTitle
        ]);
    }
    /**
     * Customer Info store
     */
    public function store(Request $request)
    {
    	$pageTitle ="Store Data";

        $validationResult =CandidateInfoValidations:: validate($request);
        
        if (!$validationResult['success']) {
            return redirect()->back();
        }                   
       
        $file_path 		= 'candidate-info';
        $image 			=  $request->file('image');		
		$skils      	=  $request->skils;

        DB::beginTransaction();
        try {
			$CandidateInfo                  = new CandidateInfo();
			$CandidateInfo->name       	    =  $request->name;
			$CandidateInfo->email       	=  $request->email;
			$CandidateInfo->gender      	=  $request->gender;
        //dd($CandidateInfo);

		 	if($image !=null && $image !=""){
                $image_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request, $file_path,'image');
            }

			$CandidateInfo->image       	=  $image_name ? $image_name : null;

			if($CandidateInfo->save()){
				$candidate_info_id = $CandidateInfo->id; 
				foreach ($skils as $key => $skil) {
					$skils                     = new Skils();
					$skils->candidate_info_id   =  $candidate_info_id;
					$skils->skils_title        =  $skil;
                    $skils->save();
				}

				GlobalFileUploadFunctoin::file_upload($request, $file_path, 'image', $image_name);
			}


			DB::commit();

        } catch (\Exception $ex) {
            DB::rollback();
            //Session::flash('danger', $e->getMessage());
        }

        return view('Admin::candidate.index', [
            'data' => $CandidateInfo,
            'message' => 'Data save successfully',
            'pageTitle' => $pageTitle
        ]);
       
    }

       /**
     * Create candidate Info
     */
    public function editCandidate($id)
    {
        $pageTitle = "Edit a candidate";
        $CandidateInfo   = CandidateInfo::with('candidateSkils')
        								->where('id',$id)->first();


        //dd($CandidateInfo->candidateSkils);

        return view('Admin::candidate.edit', [
            'pageTitle' => $pageTitle,
            'data' => $CandidateInfo,
        ]);
    }

    public function update(Request $request, $id)
    {
       
    	$validationResult =CandidateInfoValidations:: validate($request, $id);
        
        if (!$validationResult['success']) {
            return redirect()->back();
        }

        $file_path 		= 'candidate-info';
        $image 			=  $request->file('image');

        $CandidateInfo 	= CandidateInfo::find($id);
        $old_file 		= $CandidateInfo->image;
        $skils          = $request->skils;

        if (!$CandidateInfo) {
            return response([
                'success' => false,
                'message' => 'Data not found.'
            ]);
        }

        DB::beginTransaction();

        try {
			$CandidateInfo->name       	    =  $request->name??$CandidateInfo->name;
			$CandidateInfo->email       	=  $request->email??$CandidateInfo->email;
			$CandidateInfo->gender      	=  $request->gender??$CandidateInfo->gender;

		 	if($image !=null && $image !=""){
                $image_name = GlobalFileUploadFunctoin::file_validation_and_return_file_name($request,$file_path,'image');
				$CandidateInfo->image    =  $image_name;
            }

			if($CandidateInfo->save()){
				GlobalFileUploadFunctoin::file_upload($request, $file_path, 'image', $image_name, $old_file );

				$oldSkilsDelete= Skils::where('candidate_info_id',$id)->forceDelete();

		 		foreach ($skils as $key => $skil) {
					$skilsModel                		 = new Skils();
					$skilsModel->candidate_info_id   =  $id;
					$skilsModel->skils_title       	 =  $skil;
                    $skilsModel->save();
				}			 		
			 	
			}

            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();           
        }
        return redirect()->back();
        
    }

    /**
     * Customer Info moveToTrush
     */
    public function moveToTrush($id)
    {      

    	$CandidateInfo = CandidateInfo::find($id);        
        $CandidateInfo->delete();

        return redirect()->back();      
    }
    
 	
    public function restoreData($id)
    {
    	$pageTitle ="";        
        $CandidateInfo = CandidateInfo::withTrashed()
                         ->where('id', $id)
                         ->restore();
        
        return redirect()->route('infoList');
        
    }


      /**
     * Customer Info permanentlyDelete
     */
    public function permanentlyDelete($id)
    {
        $CandidateInfo = CandidateInfo::withTrashed()
                         ->where('id', $id)
                         ->forceDelete();
        
        //$CandidateInfo->forceDelete();
        return redirect()->back();
    }
}
