<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Admin\Models\Skils;
class CandidateInfo extends Model
{

    protected $table="table_candidate_info";

    use SoftDeletes;

    

    protected $fillable =[ 
		'name',	
		'email',	
		'image',	
		'gender',	
		'skils'
    ];


    public function candidateSkils(){
    	return $this->hasMany(Skils::class);
    }
}


