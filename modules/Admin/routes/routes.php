<?php
Route::Group([
    'prefix' => 'admin',
    'namespace' => 'Modules\Admin\Controllers',
    'middleware' => ['web', 'auth']], function() {

    	/*client-info routes*/
    	Route::group(['prefix'=>'/client-info'], function(){
		    Route::get('/list', 'CandidateInfoController@index')->name('infoList');
		    Route::get('/delete-list', 'CandidateInfoController@deleteList')->name('deleteList');
		    Route::get('/create', 'CandidateInfoController@createCandidate')->name('infoCreate');
		    Route::post('/store', 'CandidateInfoController@store')->name('infoStore');
		    Route::get('/edit/{id}', 'CandidateInfoController@editCandidate')->name('editCandidate');
		    
		    Route::post('/update/{id}', 'CandidateInfoController@update')->name('infoUpdate');
		    Route::get('/move-to-trush/{id}', 'CandidateInfoController@moveToTrush')->name('infoMoveToTrush');
		    Route::get('/restore-data/{id}', 'CandidateInfoController@restoreData')->name('infoRestoreData');
		    Route::get('/destroy/{id}', 'CandidateInfoController@permanentlyDelete')->name('infoPermanentlyDelete');
		});





});
