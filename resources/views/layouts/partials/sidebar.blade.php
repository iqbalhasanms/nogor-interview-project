<div class="col-md-4">
    <div class="card">
        <div class="card-header">Candidate Menu</div>
        <div class="card-body">
            <nav class="navbar bg-light">
              <ul class="navbar-nav">
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('infoList') }}"> Candidate List</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('infoCreate') }}"> Add New Candidate</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('deleteList') }}"> Delete List</a>
                </li>
              </ul>
            </nav>
        </div>
    </div>
</div>