@extends('layouts.master')
@section('title', 'Customer List')


		@section('content')
<div class="row">
	<div class="col-6 col-offset-3">
		<form method="POST" action="{{ route('infoStore') }}" enctype="multipart/form-data"> 	
		   @csrf

		    <div class="form-group">
		      <label for="name">Name:</label>
		      <input type="text" class="form-control" id="name" placeholder="Enter name" name="name">
		    </div>

		    <div class="form-group">
		      <label for="email">Email:</label>
		      <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
		    </div>

		    <div class="form-group">
		    	<input type="file" class="form-control" name="image" />
		    </div>	
		    <div class="form-group">
			    <label for="gender">Gender:</label>
			    <div  id="gender">
					<input type="radio"  name="gender" value="male"> Male
					<input type="radio"  name="gender" value="female"> Female			    	
			    </div>			      
		    </div>


		    <div class="form-group">
			    <label for="gender">Skils:</label>
			    <div  id="gender">
					<input type="checkbox"  name="skils[]" value="laravel"> Laravel
					<input type="checkbox"  name="skils[]" value="codeigniter"> Codeigniter    	
					<input type="checkbox"  name="skils[]" value="ajax"> Ajax			    	
					<input type="checkbox"  name="skils[]" value="vuejs"> Vue Js			    	
					<input type="checkbox"  name="skils[]" value="mysql"> MySql			    	
					<input type="checkbox"  name="skils[]" value="api"> Api			    	
			    </div>			      
		    </div>


		    <button type="submit" class="btn btn-primary">Submit</button>
		</form>
	</div>
</div>
		@endsection
  